<?php

use EasyCorp\Bundle\EasyDeployBundle\Deployer\DefaultDeployer;

return new class extends DefaultDeployer
{
    public function configure()
    {
        return $this->getConfigBuilder()
            // SSH connection string to connect to the remote server (format: user@host-or-IP:port-number)
            ->server('root@91.235.128.222')
            // the absolute path of the remote server directory where the project is deployed
            ->deployDir('/home/admin/web/switch-case.com.ua/public_html')
            // the URL of the Git repository where the project code is hosted
            ->repositoryUrl('git@gitlab.com:vladshut/konobaza-plugin-be.git')
            ->composerInstallFlags('--prefer-dist --no-interaction')
            // the repository branch to deploy
            ->repositoryBranch('master')
            ->sharedFilesAndDirs(['.env', 'uploads/'])
            ->symfonyEnvironment('prod')
            ->writableDirs(['var/cache'])
        ;
    }

    // run some local or remote commands before the deployment is started
    public function beforeStartingDeploy()
    {
        // $this->runLocal('./vendor/bin/simple-phpunit');
    }

    // run some local or remote commands after the deployment is finished
    public function beforeFinishingDeploy()
    {
        $this->runRemote('{{ console_bin }} about');
        $this->runRemote('{{ console_bin }} doc:mig:mig -v --allow-no-migration');
        // $this->runRemote('{{ console_bin }} app:my-task-name');
        // $this->runLocal('say "The deployment has finished."');
    }
};
