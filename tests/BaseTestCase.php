<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 16.02.19
 * Time: 21:49
 */

namespace App\Tests;


use App\Entity\Actor;
use App\Entity\Country;
use App\Entity\Genre;
use App\Entity\MediaItem;
use App\Entity\Quality;
use App\Entity\Torrent;
use App\Entity\TorrentFile;
use App\Repository\BaseRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpKernel\Client;

class BaseTestCase extends WebTestCase
{
    /** @var EntityManager */
    protected $entityManager;

    /** @var  Client $client */
    protected $client;

    public function setUp()
    {
        parent::setUp();
        self::bootKernel();
        $this->client = static::createClient();
        $this->entityManager = self::$container->get('doctrine.orm.entity_manager');
        $this->truncateEntities([
            Genre::class,
            Actor::class,
            Country::class,
            Quality::class,
            Torrent::class,
            TorrentFile::class,
            MediaItem::class,
        ]);
    }

    /**
     * @param $command
     * @return int
     * @throws \Exception
     */
    protected function runCommand($command)
    {
        $command = sprintf('%s -e test --verbose', $command);
        $output = new BufferedOutput();

        $result = $this->createApplication()->run(new StringInput($command), $output);

        echo $output->fetch();

        return $result;
    }

    /**
     * @return Application
     */
    protected function createApplication()
    {
        $application = new Application(self::$kernel);
        $application->setAutoExit(false);

        return $application;
    }

    protected function assertSeeInDatabase($entity, $criteria, $message = '') {
        $count = $this->getDatabaseCount($entity, $criteria);

        $message .=  sprintf(
            ' Unable to find row in database table [%s] that matched attributes [%s].', $entity, json_encode($criteria)
        );

        $this->assertGreaterThan(0, $count, $message);

        return $this;
    }

    protected function getDatabaseCount($entity, $criteria) {
        $qb = $this->entityManager
            ->createQueryBuilder()
            ->select('COUNT(e)')
            ->from($entity, 'e');
        foreach($criteria as $field => $value) {
            if ($value === null) {
                $qb->andWhere("e.{$field} IS NULL");
            } else if (is_array($value)){
                array_walk($value, function(&$item) {
                    $item = "'{$item}'";
                });
                $value = implode(',', $value);
                $qb->andWhere("e.{$field} IN ({$value})");
            } else {
                $qb->andWhere("e.{$field} = :{$field}")->setParameter($field, $value);
            }
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param $object
     * @return BaseRepository
     */
    protected function getRepo($object): EntityRepository
    {
        if (is_object($object)) {
            $object = get_class($object);
        }

        /** @var EntityRepository $repo */
        $repo = $this->entityManager->getRepository($object);

        return $repo;
    }

    /**
     * @param string $serviceId
     * @return mixed
     */
    protected function get(string $serviceId)
    {
        return self::$container->get($serviceId);
    }

    protected function truncateEntities(array $entities)
    {
        $connection = $this->entityManager->getConnection();
        $databasePlatform = $connection->getDatabasePlatform();
        if ($databasePlatform->supportsForeignKeyConstraints()) {
            $connection->query('SET FOREIGN_KEY_CHECKS=0');
        }
        foreach ($entities as $entity) {
            $query = $databasePlatform->getTruncateTableSQL(
                $this->entityManager->getClassMetadata($entity)->getTableName()
            );
            $connection->executeUpdate($query);
        }
        if ($databasePlatform->supportsForeignKeyConstraints()) {
            $connection->query('SET FOREIGN_KEY_CHECKS=1');
        }
    }
}