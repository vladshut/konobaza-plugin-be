<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 16.02.19
 * Time: 21:48
 */

namespace App\Tests\Service;

use App\Entity\Actor;
use App\Entity\Country;
use App\Entity\Genre;
use App\Entity\MediaItem;
use App\Entity\PosterFile;
use App\Entity\Quality;
use App\Entity\TorrentFile;
use App\Service\TolokaParser;
use App\Tests\BaseTestCase;
use App\Utils\ParserHandler\Handler\ActorsHandler;
use App\Utils\ParserHandler\Handler\CountriesHandler;
use App\Utils\ParserHandler\Handler\DescriptionHandler;
use App\Utils\ParserHandler\Handler\DurationHandler;
use App\Utils\ParserHandler\Handler\QualityHandler;
use App\Utils\ParserHandler\Handler\TitleHandler;
use App\Utils\ParserHandler\Handler\YearHandler;
use App\Utils\ParserHandler\Request\Request;
use Doctrine\Common\Collections\ArrayCollection;

class TolokaParserTest extends BaseTestCase
{
    /**
     * @test
     */
    public function getAllTorrentIdsTest()
    {
        $expectedTorrentIds = ['t98921', 't98919', 't37833', 't98915'];

        $tolokaParser = self::$container->get(TolokaParser::class);
        $content = file_get_contents(__DIR__ . '/Fixtures/films_page_1.html');
        $torrentIds = $tolokaParser->getAllTorrentIds($content);

        $this->assertEquals($expectedTorrentIds, $torrentIds, '', 0.0, 10, true);
    }

    /**
     * @test
     */
    public function getMediaTest_1()
    {
        $existingActor = (new Actor())->setName('Кевін Спейсі')->setNameEn('Kevin Spacey');
        $this->getRepo($existingActor)->save($existingActor);

        $existingGenre = (new Genre())->setName('драма');
        $this->getRepo($existingGenre)->save($existingGenre);

        $existingCountry = (new Country())->setName('США');
        $this->getRepo($existingCountry)->save($existingCountry);

        $expectedMediaItem = (new MediaItem())
            ->setPoster((new PosterFile())->setExternalLink('//toloka.to/photos/12022719132870795_f0_0.jpg'))
            ->setTitleUa('Планета Ка-ПЕКС')
            ->setTitleOrig('K-PAX')
            ->setYear(2001)
            ->setDescription('В Манхеттенський психіатричний інститут привозять дивну людину в чорних окулярах. Він називає себе Протом і стверджує, що його батьківщина - далека планета Ка-Пекс, звідки він миттєво перенісся на Землю на промені світла. Незважаючи на всі зусилля, досвідченому доктору Пауеллу не вдається розгадати загадку таємничого пацієнта, який охоче і дуже переконливо доводить всім своє позаземне походження і заздалегідь призначає дату свого повернення на Ка-ПЕКС. Незабаром Марка Пауелла, що так і не зумів відкрити таємницю особистості свого найдивнішого підопічного, починає турбувати неймовірна, але нав\'язлива думка: що може Прот зовсім не божевільний і в його словах є частка правди ...')
            ->setDuration(7218)
            ->setSize(2453667.84)
            ->setTolokaTorrentId('t99059')
            ->setImdbId('tt0272152')
            ->setActors(
                new ArrayCollection([
                    $existingActor,
                    (new Actor())->setName('Джеф Бріджес')->setNameEn('Jeff Bridges'),
                    (new Actor())->setName('Девід Патрік Келі')->setNameEn('David Patrick Kelly'),
                ])
            )
            ->setGenres(
                new ArrayCollection([
                    $existingGenre,
                    (new Genre())->setName('фантастика'),
                ])
            )
            ->setCountries(
                new ArrayCollection([
                    $existingCountry,
                    (new Country())->setName('Німеччина'),
                ])
            )
            ->setQuality((new Quality())->setName('DTheaterRip-AVC'))
            ->setTorrentFile((new TorrentFile())->setExternalLink('https://toloka.to/download.php?id=114163'))
        ;

        $tolokaParser = self::$container->get(TolokaParser::class);
        $content = file_get_contents(__DIR__ . '/Fixtures/film_t99059.html');
        $mediaItem = $tolokaParser->getMediaItem($content);

        $this->assertEquals($expectedMediaItem->jsonSerialize(), $mediaItem->jsonSerialize());
    }

    /**
     * @test
     */
    public function getMediaTest_2()
    {
        $existingActor = (new Actor())->setName('Крістіан Слейтер')->setNameEn(null);
        $this->getRepo($existingActor)->save($existingActor);

        $existingGenre = (new Genre())->setName('трилер');
        $this->getRepo($existingGenre)->save($existingGenre);

        $existingCountry = (new Country())->setName('США');
        $this->getRepo($existingCountry)->save($existingCountry);

        $expectedMediaItem = (new MediaItem())
            ->setPoster((new PosterFile())->setExternalLink('//toloka.to/files/fotky-files/16319_mfsh9.jpg'))
            ->setTitleUa('Мисливці за головами')
            ->setTitleOrig('Pursued')
            ->setYear(2004)
            ->setDescription('Після вдалої презентації свого сенсаційного винаходу, талановитий інженер Бен Кітс стає бажаною здобиччю для жорстокого та небезпечного Вінсента Палмера, екстравагантного мисливця за "розумними головами". Діючи на межі фолу, Палмер намагається переманити Бена на сторону конкурентів. Підозрюючи, що його впертий переслідувач здатен на все, Бен оголошує йому війну, ще незнаючи що йому протистоїть не просто фанатик-кар\'єрист...')
            ->setDuration(5490)
            ->setSize(1394606.08)
            ->setTolokaTorrentId('t3957')
            ->setImdbId(null)
            ->setActors(
                new ArrayCollection([
                    $existingActor,
                    (new Actor())->setName('Джил Беллоуз')->setNameEn(null),
                    (new Actor())->setName('Естелла Варрен')->setNameEn(null),
                    (new Actor())->setName('Майкл Кларк Дункан')->setNameEn(null),
                    (new Actor())->setName('Кончіта Кемпбелл')->setNameEn(null),
                ])
            )
            ->setGenres(
                new ArrayCollection([
                    $existingGenre,
                ])
            )
            ->setCountries(
                new ArrayCollection([
                    $existingCountry,
                    (new Country())->setName('Канада'),
                ])
            )
            ->setQuality((new Quality())->setName('DVDRip'))
            ->setTorrentFile((new TorrentFile())->setExternalLink('https://toloka.to/download.php?id=4264'))
        ;

        $tolokaParser = self::$container->get(TolokaParser::class);
        $content = file_get_contents(__DIR__ . '/Fixtures/film_t3957.html');
        $mediaItem = $tolokaParser->getMediaItem($content);

        $this->assertEquals($expectedMediaItem->jsonSerialize(), $mediaItem->jsonSerialize());
    }

    /**
     * @test
     */
    public function getMediaTest_3()
    {
        $existingActor = (new Actor())->setName('Леонардо ДіКапріо')->setNameEn(null);
        $this->getRepo($existingActor)->save($existingActor);

        $existingGenre = (new Genre())->setName('фантастика');
        $this->getRepo($existingGenre)->save($existingGenre);

        $existingCountry = (new Country())->setName('США');
        $this->getRepo($existingCountry)->save($existingCountry);

        $expectedMediaItem = (new MediaItem())
            ->setPoster((new PosterFile())->setExternalLink('//toloka.hurtom.com/photos/140730003846107574_f0_0.jpg'))
            ->setTitleUa('Початок')
            ->setTitleOrig('Inception')
            ->setYear(2010)
            ->setDescription('Кобб – талановитий злодій, кращий з кращих в небезпечному мистецтві вилучення. Він краде цінні секрети з глибин людського розуму, маніпулюючи свідомістю жертви під час сну, коли людський розум найбільш вразливий. Рідкісні здібності Кобба зробили його цінним гравцем у світі промислового шпигунства, але натомість вони позбавили його всього, що він любив, перетворивши в одвічного втікача. Однак доля надає йому шанс виправити помилки. Його остання справа може все повернути, але для цього він має зробити неможливе – укорінення. Тепер Кобб і його команда повинні не викрасти ідею, а навпаки, виконати процес її зародження у підсвідомість людини...')
            ->setDuration(8887)
            ->setSize(1950351.36)
            ->setTolokaTorrentId('t69717')
            ->setImdbId('tt1375666')
            ->setActors(
                new ArrayCollection([
                    $existingActor,
                    (new Actor())->setName('Джозеф Гордон-Левітт')->setNameEn(null),
                    (new Actor())->setName('Еллен Пейдж')->setNameEn(null),
                    (new Actor())->setName('Том Харді')->setNameEn(null),
                    (new Actor())->setName('Кен Ватанабе')->setNameEn(null),
                    (new Actor())->setName('Кілліан Мерфі')->setNameEn(null),
                    (new Actor())->setName('Маріон Котіяр')->setNameEn(null),
                    (new Actor())->setName('Майкл Кейн')->setNameEn(null),
                ])
            )
            ->setGenres(
                new ArrayCollection([
                    $existingGenre,
                    (new Genre())->setName('бойовик'),
                    (new Genre())->setName('трилер'),
                    (new Genre())->setName('драма'),
                    (new Genre())->setName('детектив'),
                ])
            )
            ->setCountries(
                new ArrayCollection([
                    $existingCountry,
                    (new Country())->setName('Велика Британія'),
                ])
            )
            ->setQuality((new Quality())->setName('BDRip-AVC'))
            ->setTorrentFile((new TorrentFile())->setExternalLink('https://toloka.to/download.php?id=85801'))
        ;

        $tolokaParser = self::$container->get(TolokaParser::class);
        $content = file_get_contents(__DIR__ . '/Fixtures/film_t69717.html');
        $mediaItem = $tolokaParser->getMediaItem($content);

        $this->assertEquals($expectedMediaItem->jsonSerialize(), $mediaItem->jsonSerialize());
    }

    /**
     * @test
     */
    public function getMediaTest_4()
    {
        $existingActor = (new Actor())->setName('Аль Пачіно')->setNameEn('Al Pacino');
        $this->getRepo($existingActor)->save($existingActor);

        $existingGenre = (new Genre())->setName('екшн');
        $this->getRepo($existingGenre)->save($existingGenre);

        $existingCountry = (new Country())->setName('США');
        $this->getRepo($existingCountry)->save($existingCountry);

        $existingQuality = (new Quality())->setName('BDRip');
        $this->getRepo($existingQuality)->save($existingQuality);

        $expectedMediaItem = (new MediaItem())
            ->setPoster((new PosterFile())->setExternalLink('//toloka.to/photos/13122414113459994_f0_0.jpg'))
            ->setTitleUa('Схватка')
            ->setTitleOrig('Heat')
            ->setYear(1995)
            ->setDescription('Кримінальна драма про протистояння злочинця і поліцейського. Дуже напружений і психологічний фільм режисера Майкла Манна, в якому вперше за більш ніж 20 років, такі метри кіномистецтва як Аль Пачіно і Роберт де Ніро зіграли разом. Ніл МакКолі, якого грає де Ніро, є ватажком банди грабіжників, що складається з декількох його друзів. Бандитам вдається спланувати і провести напад на броньовик, викравши понад півтора мільйона доларів у цінних паперах. Справа ускладнюється, коли новий член банди на ім\'я Вейнґро, вбиває одного з охоронців, і банді доводиться вбити інших, як свідків. Справу доручають вести лейтенанту елітного відділу поліції Вінсенту Ханні, якого грає Аль Пачіно...')
            ->setDuration(10227)
            ->setSize(3523215.36)
            ->setTolokaTorrentId('t52194')
            ->setImdbId('tt0113277')
            ->setActors(
                new ArrayCollection([
                    $existingActor,
                    (new Actor())->setName('Роберт Де Ніро')->setNameEn('Robert De Niro'),
                    (new Actor())->setName('Вел Кілмер')->setNameEn('Val Kilmer'),
                    (new Actor())->setName('Том Сайзмор')->setNameEn('Tom Sizemore'),
                    (new Actor())->setName('Емі Бреннеман')->setNameEn('Amy Brenneman'),
                    (new Actor())->setName('Джон Войт')->setNameEn('Jon Voight'),
                    (new Actor())->setName('Даян Венора')->setNameEn('Diane Venora'),
                    (new Actor())->setName('Ешлі Джадд')->setNameEn('Ashley Judd'),
                    (new Actor())->setName('Майкелті Вільямсон')->setNameEn('Mykelti Williamson'),
                    (new Actor())->setName('Вес Студі')->setNameEn('Wes Studi'),
                    (new Actor())->setName('Денні Трехо')->setNameEn('Danny Trejo'),
                    (new Actor())->setName('Наталі Портман')->setNameEn('Natalie Portman'),
                ])
            )
            ->setGenres(
                new ArrayCollection([
                    $existingGenre,
                    (new Genre())->setName('кримінальний'),
                    (new Genre())->setName('драма'),
                    (new Genre())->setName('трилер'),
                ])
            )
            ->setCountries(
                new ArrayCollection([
                    $existingCountry,
                ])
            )
            ->setQuality($existingQuality)
            ->setTorrentFile((new TorrentFile())->setExternalLink('https://toloka.to/download.php?id=66754'))
        ;

        $tolokaParser = self::$container->get(TolokaParser::class);
        $content = file_get_contents(__DIR__ . '/Fixtures/film_t52194.html');
        $mediaItem = $tolokaParser->getMediaItem($content);

        $this->assertEquals($expectedMediaItem->jsonSerialize(), $mediaItem->jsonSerialize());
    }

    /**
     * @test
     * @throws \App\Utils\ParserHandler\Exception\NotHandledException
     */
    public function qualityHandlerTest()
    {
        $content = file_get_contents(__DIR__ . '/Fixtures/film_t35187.html');
        $request = new Request($content);
        /** @var QualityHandler $handler */
        $handler = $this->get(QualityHandler::class);

        $handler->handle($request);

        $this->assertEquals('BDRip 720p', $request->getMediaItem()->getQuality()->getName());
    }

    /**
     * @test
     * @throws \App\Utils\ParserHandler\Exception\NotHandledException
     */
    public function durationHandlerTest()
    {
        $content = file_get_contents(__DIR__ . '/Fixtures/film_t1051.html');
        $request = new Request($content);
        /** @var DurationHandler $handler */
        $handler = $this->get(DurationHandler::class);

        $handler->handle($request);

        $this->assertEquals(6963, $request->getMediaItem()->getDuration());
    }

    /**
     * @test
     * @throws \App\Utils\ParserHandler\Exception\NotHandledException
     */
    public function durationHandlerTest_2()
    {
        $content = file_get_contents(__DIR__ . '/Fixtures/film_t1023.html');
        $request = new Request($content);
        /** @var DurationHandler $handler */
        $handler = $this->get(DurationHandler::class);

        $handler->handle($request);

        $this->assertEquals(6330, $request->getMediaItem()->getDuration());
    }

    /**
     * @test
     * @throws \App\Utils\ParserHandler\Exception\NotHandledException
     */
    public function titleHandlerTest()
    {
        $content = file_get_contents(__DIR__ . '/Fixtures/film_t10408.html');
        $request = new Request($content);
        /** @var TitleHandler $handler */
        $handler = $this->get(TitleHandler::class);

        $handler->handle($request);

        $this->assertEquals('Трансформери: Дилогія + Бонусні матеріали', $request->getMediaItem()->getTitleUa());
        $this->assertEquals('Transformers - Dilogy + Bonus', $request->getMediaItem()->getTitleOrig());
    }

    /**
     * @test
     * @throws \App\Utils\ParserHandler\Exception\NotHandledException
     */
    public function titleHandlerTest_2()
    {
        $content = file_get_contents(__DIR__ . '/Fixtures/film_t1368.html');
        $request = new Request($content);
        /** @var TitleHandler $handler */
        $handler = $this->get(TitleHandler::class);

        $handler->handle($request);

        $this->assertEquals('Солдати - 14 років до дембеля', $request->getMediaItem()->getTitleUa());
        $this->assertEquals(null, $request->getMediaItem()->getTitleOrig());
    }

    /**
     * @test
     * @throws \App\Utils\ParserHandler\Exception\NotHandledException
     */
    public function yearHandlerTest()
    {
        $content = file_get_contents(__DIR__ . '/Fixtures/film_t10408.html');
        $request = new Request($content);
        /** @var YearHandler $handler */
        $handler = $this->get(YearHandler::class);

        $handler->handle($request);

        $this->assertEquals(2007, $request->getMediaItem()->getYear());
    }

    /**
     * @test
     * @throws \App\Utils\ParserHandler\Exception\NotHandledException
     */
    public function countriesHandlerTest()
    {
        $content = file_get_contents(__DIR__ . '/Fixtures/film_t11834.html');
        $request = new Request($content);
        /** @var CountriesHandler $handler */
        $handler = $this->get(CountriesHandler::class);

        $handler->handle($request);

        $countries = new ArrayCollection([
            (new Country())->setName('США'),
            (new Country())->setName('Франція'),
            (new Country())->setName('Німеччина'),
            (new Country())->setName('Велика Британія'),
            (new Country())->setName('Іспанія'),
        ]);

        $this->assertEquals($countries, $request->getMediaItem()->getCountries());
    }

    /**
     * @test
     * @throws \App\Utils\ParserHandler\Exception\NotHandledException
     */
    public function descriptionHandlerTest()
    {
        $content = file_get_contents(__DIR__ . '/Fixtures/film_t1189.html');
        $request = new Request($content);
        /** @var DescriptionHandler $handler */
        $handler = $this->get(DescriptionHandler::class);

        $handler->handle($request);

        $expectedDescription = 'Рон Борґанді, найпопулярніший ведучий 70-х у Сан-Дієго, переконаний, що жінок у студію можна запрошувати тоді, коли йдеться про моду або кулінарні рецепти. Тож коли Рон дізнається, що працюватиме з молодою телеведучою - гарною, амбітною і досить розумною, щоб стати не лише окрасою екрана, - відбувається не просто зіткнення двох яскравих професіоналів, а справжня війна!!!';
        $this->assertEquals($expectedDescription, $request->getMediaItem()->getDescription());
    }

    /**
     * @test
     * @throws \App\Utils\ParserHandler\Exception\NotHandledException
     */
    public function actorsHandlerTest()
    {
        $content = file_get_contents(__DIR__ . '/Fixtures/film_t22309.html');
        $request = new Request($content);
        /** @var ActorsHandler $handler */
        $handler = $this->get(ActorsHandler::class);

        $handler->handle($request);

        $actors = new ArrayCollection([
            (new Actor())->setName('Роберт Дауні Молодший'),
            (new Actor())->setName('Зак Галіфіанакіс'),
            (new Actor())->setName('Мішель Монагам'),
            (new Actor())->setName('Джеймі Фокс'),
            (new Actor())->setName('Джульєтт Льюіс'),
        ]);

        $this->assertEquals($actors->toArray(), $request->getMediaItem()->getActors()->toArray());
    }

    /**
     * @test
     * @throws \App\Utils\ParserHandler\Exception\NotHandledException
     */
    public function actorsHandlerTest_2()
    {
        $content = file_get_contents(__DIR__ . '/Fixtures/film_t28957.html');
        $request = new Request($content);
        /** @var ActorsHandler $handler */
        $handler = $this->get(ActorsHandler::class);

        $handler->handle($request);

        $actors = new ArrayCollection([
            (new Actor())->setName('Том Генкс'),
            (new Actor())->setName('Мег Раян'),
            (new Actor())->setName('Грег Кіннер'),
            (new Actor())->setName('Паркер Поузі'),
        ]);

        $this->assertEquals($actors->toArray(), $request->getMediaItem()->getActors()->toArray());
    }
}