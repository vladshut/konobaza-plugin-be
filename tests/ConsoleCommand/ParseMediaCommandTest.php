<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 16.02.19
 * Time: 20:29
 */

namespace App\Tests\ConsoleCommand;


use App\ConsoleCommand\ParseMediaCommand;
use App\Entity\MediaItem;
use App\Entity\Torrent;
use App\Entity\TorrentFile;
use App\Repository\TorrentRepository;
use App\Service\TolokaClient;
use App\Tests\BaseTestCase;
use Doctrine\ORM\EntityManager;

class ParseMediaCommandTest extends BaseTestCase
{
    /** @var EntityManager */
    protected $entityManager;

    /**
     * @test
     * @throws \Exception
     */
    public function parseMediaCommandTest()
    {
        $tolokaTorrentId = 't99059';

        $torrent = (new Torrent())
            ->setTolokaTorrentId($tolokaTorrentId)
            ->setState(Torrent::STATE_NEW);

        $torrentRepository = self::$container->get(TorrentRepository::class);

        $torrentRepository->save($torrent);

        $expectedMediaItem = (new MediaItem())
            ->setTolokaTorrentId($tolokaTorrentId)
            ->setImdbId('tt0272152');

        $eventBusMock = $this->createMock(TolokaClient::class);
        $eventBusMock
            ->expects($this->exactly(1))
            ->method('getMediaItem')
            ->willReturnMap([[$tolokaTorrentId, $expectedMediaItem]]);

        $container = self::$container;
        $container->set(TolokaClient::class, $eventBusMock);
        $this->runCommand(ParseMediaCommand::NAME);

        $this->assertSeeInDatabase(Torrent::class, [
            'state' => Torrent::STATE_HANDLED_SUCCESS,
            'tolokaTorrentId' => $tolokaTorrentId
        ]);
        $criteria = array_only($expectedMediaItem->jsonSerialize(), ['tolokaTorrentId', 'imdbId']);
        $this->assertSeeInDatabase(MediaItem::class, $criteria);
    }
}