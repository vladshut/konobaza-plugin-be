<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 16.02.19
 * Time: 20:29
 */

namespace App\Tests\ConsoleCommand;


use App\ConsoleCommand\ParseCommand;
use App\Entity\Torrent;
use App\Service\TolokaClient;
use App\Tests\BaseTestCase;
use Doctrine\ORM\EntityManager;

class ParseCommandTest extends BaseTestCase
{
    /** @var EntityManager */
    protected $entityManager;

    /**
     * @test
     * @throws \Exception
     */
    public function parseCommandTest()
    {
        $categories = ['f16', 'f96'];
        $torrentIds = ['t4567', 't4568'];

        $returns = [];
        foreach ($torrentIds as $torrentId) {
            $returns[] = [$torrentId];
        }
        $returns[] = [];


        $returnMap = [];

        foreach ($categories as $category) {
            foreach ($returns as $page => $return) {
                $returnMap[] = [$page + 1, $category, $return];
            }
        }

        $eventBusMock = $this->createMock(TolokaClient::class);
        $eventBusMock
            ->expects($this->exactly(count($returnMap)))
            ->method('getFilmsTorrents')
            ->willReturnMap($returnMap)
        ;
        $container = self::$container;
        $container->set(TolokaClient::class, $eventBusMock);

        ParseCommand::$categories = $categories;
        ParseCommand::$timeout = 0;
        $this->runCommand(ParseCommand::NAME);

        foreach ($torrentIds as $torrentId) {
            $this->assertSeeInDatabase(Torrent::class, ['tolokaTorrentId' => $torrentId]);
        }
    }
}