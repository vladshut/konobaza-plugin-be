<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 04.03.19
 * Time: 10:04
 */

namespace App\Tests\Controller;


use App\Entity\MediaItem;
use App\Tests\BaseTestCase;
use Symfony\Component\HttpFoundation\Response;

class MediaControllerTest extends BaseTestCase
{
    /**
     * @test
     */
    public function testFind()
    {
        $this->loadFixtures();
        $requestParams = ['title' => 'Film 2', 'imdbId' => 1];
        $this->client->request('GET', '/api/media', $requestParams);
        $response = $this->client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $content = $this->client->getResponse()->getContent();
        $content = json_decode($content, true);
        $this->assertCount(3, $content);
    }

    private function loadFixtures()
    {
        $config = [
            ['titleOrig' => 'Film 1', 'imdbId' => 1],
            ['titleOrig' => 'Film 2', 'imdbId' => 1],
            ['titleOrig' => 'Film 2', 'imdbId' => null],
            ['titleOrig' => 'Film 2', 'imdbId' => 2],
            ['titleOrig' => 'Film 3', 'imdbId' => 3],
        ];

        foreach ($config as $item) {
            $mi = (new MediaItem())
                ->setTitleOrig($item['titleOrig'])
                ->setImdbId($item['imdbId'])
                ->setTolokaTorrentId('1');
            $this->getRepo($mi)->save($mi);
        }
    }
}