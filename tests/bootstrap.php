<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 17.07.18
 * Time: 13:11
 */
require __DIR__.'/../config/bootstrap.php';

passthru(sprintf(
    'php "%s/../bin/console" doctrine:database:drop --force --env=test --quiet',
    __DIR__
));


passthru(sprintf(
    'php "%s/../bin/console" doctrine:database:create --env=test --quiet',
    __DIR__
));

passthru(sprintf(
    'php "%s/../bin/console" doctrine:migrations:migrate --env=test --quiet',
    __DIR__
));
