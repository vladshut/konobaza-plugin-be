<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 24.09.18
 * Time: 14:35
 */

namespace App\ConsoleCommand;

use App\Entity\Torrent;
use App\Repository\TorrentRepository;
use App\Service\TolokaClient;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ParseCommand extends Command
{
    use LockableTrait;

    const NAME = 'app:parse';

    public static $categories = ['f16', 'f96', 'f19', 'f144', 'f139', 'f140'];
    public static $timeout = 2;

    /** @var TolokaClient */
    private $client;
    /** @var TorrentRepository */
    private $torrentRepo;

    public function __construct(TolokaClient $client, TorrentRepository $torrentRepo)
    {
        parent::__construct();

        $this->client = $client;
        $this->torrentRepo = $torrentRepo;
    }

    protected function configure()
    {

        $this
            ->setName(self::NAME)
            ->setDescription('Parse https://toloka.to/')
            ->setHelp('This command parse site https://toloka.to/ and store data to database')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $this->lock(null, true);

        foreach (self::$categories as $category) {
            $output->writeln("CATEGORY: {$category}");
            $this->parseCategory($category, $output);
        }

        $this->release();
    }

    private function parseCategory(string $category, OutputInterface $output) {
        $page = 1;
        $bumpIntoExists = 0;
        $imported = 0;

        do {
            $torrentIds = $this->client->getFilmsTorrents($page, $category);

            foreach ($torrentIds as $torrentId) {
                if ($this->torrentRepo->findOneBy(['tolokaTorrentId' => $torrentId])) {
                    $bumpIntoExists ++;
                    continue;
                }

                $torrent = (new Torrent())->setTolokaTorrentId($torrentId);
                $this->torrentRepo->save($torrent);
                $imported ++;
            }

            $output->writeln("Page: {$page}. Imported: {$imported}");
            $page ++;
            sleep(self::$timeout);
        } while (!empty($torrentIds) && $bumpIntoExists < 100);
    }
}
