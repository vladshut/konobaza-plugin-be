<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 17.02.19
 * Time: 8:50
 */

namespace App\ConsoleCommand;


use App\Entity\Torrent;
use App\Repository\MediaItemRepository;
use App\Repository\TorrentRepository;
use App\Service\TolokaClient;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ParseMediaCommand extends Command
{
    use LockableTrait;

    const NAME = 'app:parse-media';

    /** @var TolokaClient */
    private $client;
    /** @var TorrentRepository */
    private $torrentRepo;
    /** @var MediaItemRepository */
    private $mediaItemRepo;

    public function __construct(
        TolokaClient $client,
        TorrentRepository $torrentRepo,
        MediaItemRepository $mediaItemRepo
    ) {
        parent::__construct();

        $this->client = $client;
        $this->torrentRepo = $torrentRepo;
        $this->mediaItemRepo = $mediaItemRepo;
    }

    protected function configure()
    {

        $this
            ->setName(self::NAME)
            ->setDescription('Parse https://toloka.to/t****')
            ->setHelp('This command parse page with media on site https://toloka.to/t**** and store data to database')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        ini_set('max_execution_time', 0);

        $this->lock(null, true);

        $criteria = ['state' => Torrent::STATE_NEW];
        $orderBy = ['tolokaTorrentId' => 'asc'];

        /** @var Torrent $torrent */
        while ($torrent = $this->torrentRepo->findOneBy($criteria, $orderBy)) {
            $id = $torrent->getTolokaTorrentId();
            $output->writeln("");
            $output->writeln("----------");
            $output->writeln("");
            $output->writeln("Start handling of torrent: $id");
            try {
                $tolokaTorrentId = $torrent->getTolokaTorrentId();
                $mediaItem = $this->client->getMediaItem($tolokaTorrentId);

                $this->mediaItemRepo->save($mediaItem);

                $output->writeln("Finish handling of torrent: $id");

                $newState = Torrent::STATE_HANDLED_SUCCESS;
                $torrent->setState($newState);
                $this->torrentRepo->save($torrent);
            } catch (Exception $e) {
                $output->writeln("Error while handling of torrent: $id Error: " . $e->getMessage());
                $newState = Torrent::STATE_HANDLED_ERROR;
                $torrent->setState($newState);
                $this->torrentRepo->save($torrent);

                if ($e->getMessage() === 'The current node list is empty.') {
                    continue;
                }

                throw $e;
            }
        }

        $this->release();
    }
}