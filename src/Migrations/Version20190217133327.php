<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190217133327 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE media_item ADD torrent_file_external_url VARCHAR(255) NOT NULL, CHANGE torrent_file_id torrent_file_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE media_item ADD CONSTRAINT FK_DC5CFACD52208A4B FOREIGN KEY (torrent_file_id) REFERENCES file (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DC5CFACD52208A4B ON media_item (torrent_file_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE media_item DROP FOREIGN KEY FK_DC5CFACD52208A4B');
        $this->addSql('DROP INDEX UNIQ_DC5CFACD52208A4B ON media_item');
        $this->addSql('ALTER TABLE media_item DROP torrent_file_external_url, CHANGE torrent_file_id torrent_file_id VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
