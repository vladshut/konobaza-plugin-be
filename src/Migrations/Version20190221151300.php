<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190221151300 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE media_item_genre (media_item_id INT NOT NULL, genre_id INT NOT NULL, INDEX IDX_A3D2E6BC73B8D417 (media_item_id), INDEX IDX_A3D2E6BC4296D31F (genre_id), PRIMARY KEY(media_item_id, genre_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media_item_country (media_item_id INT NOT NULL, country_id INT NOT NULL, INDEX IDX_31DF45BE73B8D417 (media_item_id), INDEX IDX_31DF45BEF92F3E70 (country_id), PRIMARY KEY(media_item_id, country_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media_item_actor (media_item_id INT NOT NULL, actor_id INT NOT NULL, INDEX IDX_64F783BD73B8D417 (media_item_id), INDEX IDX_64F783BD10DAF24A (actor_id), PRIMARY KEY(media_item_id, actor_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE genre (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE country (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE actor (id INT AUTO_INCREMENT NOT NULL, name_en VARCHAR(255) DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE quality (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE media_item_genre ADD CONSTRAINT FK_A3D2E6BC73B8D417 FOREIGN KEY (media_item_id) REFERENCES media_item (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE media_item_genre ADD CONSTRAINT FK_A3D2E6BC4296D31F FOREIGN KEY (genre_id) REFERENCES genre (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE media_item_country ADD CONSTRAINT FK_31DF45BE73B8D417 FOREIGN KEY (media_item_id) REFERENCES media_item (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE media_item_country ADD CONSTRAINT FK_31DF45BEF92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE media_item_actor ADD CONSTRAINT FK_64F783BD73B8D417 FOREIGN KEY (media_item_id) REFERENCES media_item (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE media_item_actor ADD CONSTRAINT FK_64F783BD10DAF24A FOREIGN KEY (actor_id) REFERENCES actor (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE media_item ADD poster_id INT DEFAULT NULL, ADD quality_id INT DEFAULT NULL, ADD imdb_rating DOUBLE PRECISION DEFAULT NULL, ADD title_orig VARCHAR(255) DEFAULT NULL, ADD title_ua VARCHAR(255) DEFAULT NULL, ADD year INT DEFAULT NULL, ADD description LONGTEXT DEFAULT NULL, ADD duration INT DEFAULT NULL, ADD size INT DEFAULT NULL, CHANGE torrent_file_external_url torrent_file_external_url VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE media_item ADD CONSTRAINT FK_DC5CFACD5BB66C05 FOREIGN KEY (poster_id) REFERENCES file (id)');
        $this->addSql('ALTER TABLE media_item ADD CONSTRAINT FK_DC5CFACDBCFC6D57 FOREIGN KEY (quality_id) REFERENCES quality (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DC5CFACD5BB66C05 ON media_item (poster_id)');
        $this->addSql('CREATE INDEX IDX_DC5CFACDBCFC6D57 ON media_item (quality_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE media_item_genre DROP FOREIGN KEY FK_A3D2E6BC4296D31F');
        $this->addSql('ALTER TABLE media_item_country DROP FOREIGN KEY FK_31DF45BEF92F3E70');
        $this->addSql('ALTER TABLE media_item_actor DROP FOREIGN KEY FK_64F783BD10DAF24A');
        $this->addSql('ALTER TABLE media_item DROP FOREIGN KEY FK_DC5CFACDBCFC6D57');
        $this->addSql('DROP TABLE media_item_genre');
        $this->addSql('DROP TABLE media_item_country');
        $this->addSql('DROP TABLE media_item_actor');
        $this->addSql('DROP TABLE genre');
        $this->addSql('DROP TABLE country');
        $this->addSql('DROP TABLE actor');
        $this->addSql('DROP TABLE quality');
        $this->addSql('ALTER TABLE media_item DROP FOREIGN KEY FK_DC5CFACD5BB66C05');
        $this->addSql('DROP INDEX UNIQ_DC5CFACD5BB66C05 ON media_item');
        $this->addSql('DROP INDEX IDX_DC5CFACDBCFC6D57 ON media_item');
        $this->addSql('ALTER TABLE media_item DROP poster_id, DROP quality_id, DROP imdb_rating, DROP title_orig, DROP title_ua, DROP year, DROP description, DROP duration, DROP size, CHANGE torrent_file_external_url torrent_file_external_url VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
