<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 24.02.19
 * Time: 9:26
 */

namespace App\Utils\ParserHandler\Handler;


use App\Entity\Country;
use Doctrine\Common\Collections\ArrayCollection;
use App\Utils\ParserHandler\Request\RequestInterface;

class CountriesHandler extends AbstractHandler
{
    protected $isStrict = false;

    protected function doHandle(RequestInterface $request): bool
    {
        $content = $request->getContent();
        $content = preg_replace('/<head>.+<\/head>/s', '', $content);

        $pattern = "/Країна:(?<countries>.+?)<br\s?\/?>/s";
        preg_match($pattern, $content,$matches);
        $countriesString = trim(strip_tags($matches['countries'] ?? ''));
        $countriesString = preg_replace('/[^a-яії\'єґ0-9, \/]/iu', '', $countriesString);
        $countriesString = trim($countriesString);
        $countriesNames = explode(',', $countriesString);
        $countriesNames = array_map('trim', $countriesNames);
        $countriesNames = array_unique($countriesNames);

        $countries = new ArrayCollection();
        $countryRepo = $this->getRepo(Country::class);

        foreach ($countriesNames as &$countryName) {
            $countryName = trim($countryName);
            $country = $countryRepo->findOneBy(['name' => $countryName]) ?? (new Country())->setName($countryName);
            $countries->add($country);
        }
        unset($countryName);

        if (!$countries->count()) {
            return false;
        }

        $request->getMediaItem()->setCountries($countries);

        return true;
    }
}