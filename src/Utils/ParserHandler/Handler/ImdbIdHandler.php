<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 24.02.19
 * Time: 9:26
 */

namespace App\Utils\ParserHandler\Handler;
use App\Utils\ParserHandler\Request\RequestInterface;


class ImdbIdHandler extends AbstractHandler
{
    protected $isStrict = false;

    protected function doHandle(RequestInterface $request): bool
    {
        $content = $request->getContent();

        $pattern = "/href=\"https?:\/\/www\.imdb\.com\/title\/(?<id>tt\d+)\/\"/";
        preg_match($pattern, $content,$matches);

        $imdbId = $matches['id'] ?? null;

        if (!$imdbId) {
            return false;
        }

        $request->getMediaItem()->setImdbId($imdbId);

        return true;
    }
}