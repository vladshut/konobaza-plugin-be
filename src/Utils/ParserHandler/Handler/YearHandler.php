<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 24.02.19
 * Time: 9:26
 */

namespace App\Utils\ParserHandler\Handler;
use App\Utils\ParserHandler\Request\RequestInterface;


class YearHandler extends AbstractHandler
{
    protected function doHandle(RequestInterface $request): bool
    {
        $content = $request->getContent();

        $pattern = "/<title>.+\((?<year>\d{4}).+<\/title>/";
        preg_match($pattern, $content,$matches);

        $year = trim($matches['year'] ?? '');

        if (!$year) {
            return false;
        }

        $request->getMediaItem()->setYear($year);

        return true;
    }
}