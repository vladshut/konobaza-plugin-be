<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 24.02.19
 * Time: 9:26
 */

namespace App\Utils\ParserHandler\Handler;
use App\Utils\ParserHandler\Request\RequestInterface;


class TitleHandler extends AbstractHandler
{
    protected function doHandle(RequestInterface $request): bool
    {
        $content = $request->getContent();

        $pattern = "/<title>(?<titleUa>.+)\s?\/ (?<titleOrig>.+) \(\d{4}.+<\/title>/";
        preg_match($pattern, $content,$matches);

        $titleOrig = trim($matches['titleOrig'] ?? '');
        $titleUa = trim(explode('/', trim($matches['titleUa'] ?? $titleOrig))[0] ?? '');

        if (!$titleUa) {
            $pattern = "/<title>(?<titleUa>.+)\s?\(\d{4}.+<\/title>/";
            preg_match($pattern, $content,$matches);
            $titleUa = trim($matches['titleUa'] ?? '');
        }

        if (!$titleUa) {
            return false;
        }

        $request->getMediaItem()->setTitleUa($titleUa);
        $request->getMediaItem()->setTitleOrig($titleOrig);

        return true;
    }
}