<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 24.02.19
 * Time: 9:26
 */

namespace App\Utils\ParserHandler\Handler;


use App\Entity\Quality;
use App\Utils\ParserHandler\Request\RequestInterface;


class QualityHandler extends AbstractHandler
{
    protected $isStrict = false;

    protected function doHandle(RequestInterface $request): bool
    {
        $content = $request->getContent();

        $patternTitle = "/<title>(?<title>.+)<\/title>/";
        preg_match($patternTitle, $content,$matchesTitle);
        $title = $matchesTitle['title'] ?? '';

        $patternQuality = "/((?:PPV\.)?[HP]DTV|(?:HD)?CAM|B[DR]Rip|(?:HD-?)?TS|(?:PPV )?WEB-?DL(?: DVDRip)?|HDRip|DVDRip|DVDRIP|CamRip|SATRip|TVRip|DTheaterRip|WebRip|W[EB]BRip|BluRay|DvDScr|hdtv|telesync)/";
        preg_match($patternQuality, $title,$matchesQuality);

        $qualityStr = $matchesQuality[0] ?? '';


        $patternQualityAlt = "/Якість:(?<quality>.+?)<br\s?\/>/s";
        preg_match($patternQualityAlt, $content,$matchesQualityAlt);
        $qualityStrAlt = explode(' ', trim(strip_tags($matchesQualityAlt['quality'] ?? '')))[0] ?? '';

        $qualityStr = (strlen($qualityStr) >= strlen($qualityStrAlt)) ? $qualityStr : $qualityStrAlt;

        $patternResolution = "/([0-9]{3,4}p)/";
        preg_match($patternResolution, $title,$matchesResolution);
        $resolution = $matchesResolution[0] ?? '';

        if ($qualityStr) {
            $qualityStr .= $resolution ? (' ' . $resolution) : '';
        } else {
            $qualityStr = $resolution ?: '';
        }

        if (!$qualityStr) {
            return false;
        }

        $existingQuality = $this->getRepo(Quality::class)->findOneBy(['name' => $qualityStr]);
        $quality = $existingQuality ?? (new Quality())->setName($qualityStr);
        $request->getMediaItem()->setQuality($quality);

        return true;
    }
}