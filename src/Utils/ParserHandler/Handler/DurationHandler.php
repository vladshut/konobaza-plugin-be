<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 24.02.19
 * Time: 9:26
 */

namespace App\Utils\ParserHandler\Handler;
use App\Utils\ParserHandler\Request\RequestInterface;


class DurationHandler extends AbstractHandler
{
    protected $isStrict = false;

    protected function doHandle(RequestInterface $request): bool
    {
        $pattern = "/Тривалість.+(?<duration>\d+:[0-5]?\d:[0-5]?\d)/";
        preg_match($pattern, $request->getContent(),$matches);

        if (!isset($matches['duration'])) {
            $pattern = "/(?<duration>\d+:[0-5]?\d:[0-5]?\d)/";
            preg_match($pattern, $request->getContent(),$matches);
        }

        $parts = isset($matches['duration']) ? explode(':', $matches['duration']) : [];
        $parts = array_reverse($parts);
        $seconds = 0;

        foreach ($parts as $i => $part) {
            $seconds += pow(60, $i) * (int)(ltrim($part, '0'));
        }

        if (!$seconds) {
            return false;
        }

        $request->getMediaItem()->setDuration($seconds);

        return true;
    }
}