<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 24.02.19
 * Time: 9:26
 */

namespace App\Utils\ParserHandler\Handler;


use App\Entity\Genre;
use Doctrine\Common\Collections\ArrayCollection;
use App\Utils\ParserHandler\Request\RequestInterface;


class GenresHandler extends AbstractHandler
{
    protected $isStrict = false;

    protected function doHandle(RequestInterface $request): bool
    {
        $content = $request->getContent();
        $content = preg_replace('/<head>.+<\/head>/s', '', $content);

        $pattern = "/Жанр:(?<genres>.+?)<br\s?\/>/s";
        preg_match($pattern, $content,$matches);

        if (!isset($matches['genres'])) {
            return false;
        }

        $genresString = trim(strip_tags($matches['genres']));
        $genresString = preg_replace('/[^a-яії\'єґ0-9, \/]/iu', '', $genresString);
        $genresString = trim($genresString);
        $genresNames = explode(',', $genresString);
        $genresNames = array_unique($genresNames);

        $genres = new ArrayCollection();

        $genreRepo = $this->getRepo(Genre::class);

        foreach ($genresNames as &$genreName) {
            $genreName = trim($genreName);
            $genre = $genreRepo->findOneBy(['name' => $genreName]) ?? (new Genre())->setName($genreName);
            $genres->add($genre);
        }
        unset($genreName);

        if (!$genres->count()) {
            return false;
        }

        $request->getMediaItem()->setGenres($genres);

        return true;
    }
}