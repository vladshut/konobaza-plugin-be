<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 24.02.19
 * Time: 9:26
 */

namespace App\Utils\ParserHandler\Handler;

use App\Entity\TorrentFile;
use Symfony\Component\DomCrawler\Crawler;
use App\Utils\ParserHandler\Request\RequestInterface;


class TorrentFileHandler extends AbstractHandler
{
    protected function doHandle(RequestInterface $request): bool
    {
        $crawler = new Crawler($request->getContent());

        $downloadLink = $crawler->filter('a.piwik_download')->first()->attr('href');
        $downloadLink = $this->fixLink($downloadLink, $request);

        if (!$downloadLink) {
            return false;
        }

        $torrentFile = (new TorrentFile())->setExternalLink($downloadLink);
        $request->getMediaItem()->setTorrentFile($torrentFile);

        return true;
    }
}