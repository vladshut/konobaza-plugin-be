<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 24.02.19
 * Time: 9:26
 */

namespace App\Utils\ParserHandler\Handler;

use Symfony\Component\DomCrawler\Crawler;
use App\Utils\ParserHandler\Request\RequestInterface;


class SizeHandler extends AbstractHandler
{
    protected function doHandle(RequestInterface $request): bool
    {
        $crawler = new Crawler($request->getContent());
        $sizeSelector = 'table.btTbl tr.row4_to:nth-child(4) td:nth-child(2)';
        $size = $crawler->filter($sizeSelector)->first()->text();
        $size = str_replace(' ', '', strip_tags($size));
        $size = preg_replace('/[^A-Za-z0-9\-]/', '', $size);
        $size = (float)$this->convertBytes($this->convertToBytes($size) / 100, 'K');

        if (!$size) {
            return false;
        }

        $request->getMediaItem()->setSize($size);

        return true;
    }
}