<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 24.02.19
 * Time: 9:26
 */

namespace App\Utils\ParserHandler\Handler;


use App\Entity\PosterFile;
use App\Utils\ParserHandler\Request\RequestInterface;
use Symfony\Component\DomCrawler\Crawler;


class PosterHandler extends AbstractHandler
{
    protected function doHandle(RequestInterface $request): bool
    {
        $crawler = new Crawler($request->getContent());

        $linkSelector = '.postbody:first-child img:first-child';
        $link = $crawler->filter($linkSelector)->first()->attr('src');
        $link = $link ? $this->fixLink($link, $request) : '';

        if (!$link) {
            return false;
        }

        $poster = (new PosterFile())->setExternalLink($this->fixLink($link, $request));
        $request->getMediaItem()->setPoster($poster);

        return true;
    }
}
