<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 24.02.19
 * Time: 9:26
 */

namespace App\Utils\ParserHandler\Handler;

use Symfony\Component\DomCrawler\Crawler;
use App\Utils\ParserHandler\Request\RequestInterface;


class TolokaTorrentIdHandler extends AbstractHandler
{
    protected function doHandle(RequestInterface $request): bool
    {
        $crawler = new Crawler($request->getContent());

        $torrentLink = $crawler->filter('a.maintitle')->first()->attr('href');
        $torrentLink = $this->fixLink($torrentLink, $request);
        $needle = $request->getSchema() . $request->getDomain() . '/';
        $torrentId = str_replace([$needle, '/'], '', $torrentLink);

        if (!$torrentId) {
            return false;
        }

        $request->getMediaItem()->setTolokaTorrentId($torrentId);

        return true;
    }
}