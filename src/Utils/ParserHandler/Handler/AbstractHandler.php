<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 24.02.19
 * Time: 9:01
 */

namespace App\Utils\ParserHandler\Handler;


use App\Repository\BaseRepository;
use App\Service\RepositoryRegistry;
use App\Utils\ParserHandler\Exception\NotHandledException;
use App\Utils\ParserHandler\Request\RequestInterface;
use Psr\Log\LoggerInterface;

abstract class AbstractHandler implements HandlerInterface
{
    /** @var bool */
    protected $isStrict = true;

    /** @var RepositoryRegistry */
    private $repoRegistry;

    /** @var HandlerInterface */
    private $nextHandler;

    /** @var LoggerInterface */
    private $logger;

    /**
     * AbstractHandler constructor.
     * @param RepositoryRegistry $repoRegistry
     * @param LoggerInterface $logger
     */
    public function __construct(RepositoryRegistry $repoRegistry, LoggerInterface $logger)
    {
        $this->repoRegistry = $repoRegistry;
        $this->logger = $logger;
    }

    public function setStrict(bool $isStrict)
    {
        $this->isStrict = $isStrict;

        return $this;
    }

    public function isStrict(): bool
    {
        return $this->isStrict;
    }

    public function setNext(HandlerInterface $handler): HandlerInterface
    {
        $this->nextHandler = $handler;

        return $handler;
    }

    /**
     * @param RequestInterface $request
     * @return RequestInterface
     * @throws NotHandledException
     */
    public function handle(RequestInterface $request): RequestInterface
    {
        if (!$this->doHandle($request)) {
            $id = $request->getMediaItem()->getTolokaTorrentId();
            $handlerClass = get_class($this);
            $errorMessage = "Torrent with ID '{$id}' is not handled by '{$handlerClass}'";

            if ($this->isStrict) {
                $this->logger->warning("Error! $errorMessage");
                throw new NotHandledException("Error! $errorMessage");
            } else {
                $this->logger->warning("Warning! $errorMessage");
            }
        }

        return $this->nextHandler ? $this->nextHandler->handle($request) : $request;
    }

    protected function fixLink($link, RequestInterface $request): string
    {
        $pattern = '/[&|?]sid=.{0,32}/';
        $link =  preg_replace($pattern, '', $link);

        $availablePrefix = ['http', 'www.', '//'];

        foreach ($availablePrefix as $prefix) {
            if (substr($link, 0, strlen($prefix)) === $prefix) {
                return $link;
            }
        }

        $schema = $request->getSchema();
        $domain = $request->getDomain();
        $schemaDomain = $schema . $domain;
        $schemaDomainSlash = $schema . $domain . '/';

        if (substr($link, 0, strlen($domain)) === $domain) {
            $link = $schema . $link;
        } else if (substr($link, 0, 1) === '/') {
            $link = $schemaDomain . $link;
        } else if (substr($link, 0, strlen($schemaDomainSlash)) !== $schemaDomainSlash) {
            $link = $schemaDomainSlash . $link;
        }

        return $link;
    }

    abstract protected function doHandle(RequestInterface $request): bool;

    protected function convertToBytes(string $from): ?int {
        $units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
        $number = substr($from, 0, -2);
        $suffix = strtoupper(substr($from,-2));

        //B or no suffix
        if(is_numeric(substr($suffix, 0, 1))) {
            return preg_replace('/[^\d]/', '', $from);
        }

        $exponent = array_flip($units)[$suffix] ?? null;
        if($exponent === null) {
            return null;
        }

        return $number * (1024 ** $exponent);
    }

    protected function convertBytes($bytes, $to) {
        $formulas = [
            'K' => $bytes / 1024,
            'M' => $bytes / 1048576,
            'G' => $bytes / 1073741824,
        ];

        return $formulas[$to] ?? 0;
    }

    /**
     * @param $object
     * @return \App\Repository\BaseRepository
     */
    protected function getRepo($object): BaseRepository
    {
        return $this->repoRegistry->get($object);
    }
}