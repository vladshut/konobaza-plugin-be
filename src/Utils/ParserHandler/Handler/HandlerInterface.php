<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 24.02.19
 * Time: 9:00
 */

namespace App\Utils\ParserHandler\Handler;


use App\Utils\ParserHandler\Request\RequestInterface;

interface HandlerInterface
{
    public function setNext(HandlerInterface $handler): HandlerInterface;

    public function handle(RequestInterface $request): RequestInterface;

    public function isStrict(): bool;
}
