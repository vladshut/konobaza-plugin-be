<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 24.02.19
 * Time: 9:26
 */

namespace App\Utils\ParserHandler\Handler;

use App\Utils\ParserHandler\Request\RequestInterface;


class DescriptionHandler extends AbstractHandler
{
    protected $isStrict = false;

    protected function doHandle(RequestInterface $request): bool
    {
        $content = $request->getContent();
        $content = preg_replace('/<head>.+<\/head>/s', '', $content);

        $pattern = "/(?:Сюжет|Сюжет фільму|Про фільм):?.*<.*>(?<description>.+)<\/.*>/sU";
        preg_match($pattern, $content,$matches);
        $description = trim(strip_tags($matches['description'] ?? ''));
        $description = str_replace("\n", ' ', $description);
        $description = preg_replace('/\s\s+/', ' ', $description);
        $description = trim($description);

        if (!$description) {
            return false;
        }

        $request->getMediaItem()->setDescription($description);

        return true;
    }
}