<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 24.02.19
 * Time: 9:26
 */

namespace App\Utils\ParserHandler\Handler;


use App\Entity\Actor;
use Doctrine\Common\Collections\ArrayCollection;
use App\Utils\ParserHandler\Request\RequestInterface;

class ActorsHandler extends AbstractHandler
{
    protected $isStrict = false;

    protected function doHandle(RequestInterface $request): bool
    {
        $pattern = "/Актори:(?<actors>.+?)<[b|h]r\s?\/>/s";
        preg_match($pattern, $request->getContent(),$matches);
        $actorsString = trim(strip_tags($matches['actors'] ?? ''));
        $actorsString = preg_replace('/[^a-za-яії\'єґ0-9,\- \/]/iu', '', $actorsString);
        $actorsString = str_replace(['та інші'], '', $actorsString);
        $actorsString = trim($actorsString);
        $actorsNames = array_unique(explode(',', $actorsString));

        $actors = new ArrayCollection();
        $usedNames = [];

        foreach ($actorsNames as &$actorName) {
            $actorName = trim($actorName);
            $names = explode('/', $actorName);
            $nameUa = trim($names[0] ?? '');
            $nameEn = trim($names[1] ?? '');


            if (!$nameUa || strlen($nameUa) > 255 || in_array($nameUa, $usedNames) || ($nameEn && in_array($nameEn, $usedNames))) {
                continue;
            }

            $actorUa = $nameUa ? $this->getRepo(Actor::class)->findOneBy(['name' => $nameUa]) : null;
            $actorEn = $nameEn ? $this->getRepo(Actor::class)->findOneBy(['nameEn' => $nameEn]) : null;

            $actor = $actorUa ?? $actorEn ?? (new Actor())->setName($nameUa)->setNameEn($nameEn);

            $actors->add($actor);

            $usedNames[] = $nameUa;

            if ($nameEn) {
                $usedNames[] = $nameEn;
            }
        }
        unset($actorName);

        if (!$actors->count()) {
            return false;
        }

        $request->getMediaItem()->setActors($actors);

        return true;
    }
}