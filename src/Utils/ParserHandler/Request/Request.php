<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 24.02.19
 * Time: 9:06
 */

namespace App\Utils\ParserHandler\Request;


use App\Entity\MediaItem;

class Request implements RequestInterface
{
    protected $mediaItem;

    protected $content;

    public function __construct(string $content)
    {
        $this->mediaItem = new MediaItem();
        $this->content = $content;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getMediaItem(): MediaItem
    {
        return $this->mediaItem;
    }

    public function getSchema(): string
    {
        return 'https://';
    }

    public function getDomain(): string
    {
        return 'toloka.to';
    }
}