<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 24.02.19
 * Time: 9:03
 */

namespace App\Utils\ParserHandler\Request;


use App\Entity\MediaItem;

interface RequestInterface
{
    public function __construct(string $content);

    public function getSchema(): string;

    public function getDomain(): string;

    public function getContent(): string;

    public function getMediaItem(): MediaItem;
}