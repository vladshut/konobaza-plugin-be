<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 27.02.19
 * Time: 20:14
 */

namespace App\Service;



use App\Utils\ParserHandler\Handler\HandlerInterface;
use App\Utils\ParserHandler\Handler\TolokaTorrentIdHandler;
use App\Utils\ParserHandler\Request\RequestInterface;

class ParsingHandlersChain
{
    /** @var HandlerInterface[] */
    private $handlers;

    /**
     * ParsingHandlersChain constructor.
     * @param HandlerInterface[] $handlers
     */
    public function __construct($handlers)
    {
        foreach ($handlers as $handler) {
            $this->handlers[get_class($handler)] = $handler;
        }
    }

    public function handle(RequestInterface $request)
    {
        $handlers = $this->handlers;

        $handlers[TolokaTorrentIdHandler::class]->handle($request);

        unset($handlers[TolokaTorrentIdHandler::class]);

        foreach ($handlers as $handler) {
            if ($handler->isStrict()) {
                $handler->handle($request);
            }
        }

        foreach ($handlers as $handler) {
            if (!$handler->isStrict()) {
                $handler->handle($request);
            }
        }

        return $request;
    }
}