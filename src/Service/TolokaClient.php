<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 16.02.19
 * Time: 17:15
 */

namespace App\Service;


use App\Entity\MediaItem;
use Goutte\Client;
use Ramsey\Uuid\Uuid;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\File\File;

class TolokaClient
{
    /** @var Client */
    private $client;
    /** @var TolokaParser */
    private $parser;
    private $userName = 'vladyslav.shut@gmail.com';
    private $password = 'newwarrior2929';

    public function __construct(TolokaParser $parser)
    {
        $this->parser = $parser;
        $this->client = new Client();
        $this->logIn();
    }

    public function getFilmsTorrents(int $page, string $category): array
    {
        $pageParam = '';

        if ($page > 1) {
            $pageParam = '-' . (($page - 1) * 45);
        }

        $this->client->request('GET', "https://toloka.to/{$category}{$pageParam}?sort=8");

        /** @var Response $response */
        $response = $this->client->getResponse();
        $content = $response->getContent();
        $torrentsIds = $this->parser->getAllTorrentIds($content);

        return $torrentsIds;
    }

    /**
     * @param string $torrentId
     * @return MediaItem|null
     * @throws \Exception
     */
    public function getMediaItem(string $torrentId): ?MediaItem
    {
        $mediaItem = null;
        $this->client->request('GET', "https://toloka.to/{$torrentId}");

        /** @var Response $response */
        $response = $this->client->getResponse();
        $content = $response->getContent();
        $mediaItem = $this->parser->getMediaItem($content);

        $this->setFileToMediaItem($mediaItem);
        $this->setPosterToMediaItem($mediaItem);

        return $mediaItem;
    }

    /**
     * @param MediaItem $mediaItem
     * @throws \Exception
     */
    private function setFileToMediaItem(MediaItem $mediaItem)
    {
        $downloadLink = $mediaItem->getTorrentFile()->getExternalLink();

        $fileName = Uuid::uuid4()->toString();
        $pathToSave = '/tmp/' . $fileName;
        $file = fopen($pathToSave, "w");

        $this->client->setClient(new \GuzzleHttp\Client(['timeout' => 5]));

        try {
            $this->client->request('GET', $downloadLink);
        } catch (\Exception $e) {
            $mediaItem->setPoster(null);
            return;
        }

        $fileContent = $this->client->getResponse()->getContent();

        fwrite($file, $fileContent);

        $mimeType = mime_content_type($pathToSave);
        $uploadedFile = new File($pathToSave, $fileName, $mimeType);
        $mediaItem->getTorrentFile()->setFile($uploadedFile);
    }

    /**
     * @param MediaItem $mediaItem
     * @throws \Exception
     */
    private function setPosterToMediaItem(MediaItem $mediaItem)
    {
        $posterLink = $mediaItem->getPoster()->getExternalLink();

        $fileName = Uuid::uuid4()->toString();
        $pathToSave = '/tmp/' . $fileName;
        $file = fopen($pathToSave, "w");

        $this->client->setClient(new \GuzzleHttp\Client(['timeout' => 5]));

        try {
            $this->client->request('GET', $posterLink);
        } catch (\Exception $e) {
            $mediaItem->setPoster(null);
            return;
        }

        $fileContent = $this->client->getResponse()->getContent();

        fwrite($file, $fileContent);

        $mimeType = mime_content_type($pathToSave);
        $uploadedFile = new File($pathToSave, $fileName, $mimeType);
        $mediaItem->getPoster()->setFile($uploadedFile);
    }

    private function logIn() {
        $crawler = $this->client->request('GET', 'https://toloka.to/login.php');
        $form = $crawler->selectButton('Вхід')->form();

        $credentials = [
            'username' => $this->userName,
            'password' => $this->password
        ];

        $this->client->submit($form, $credentials);
    }
}