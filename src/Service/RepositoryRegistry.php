<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 27.08.18
 * Time: 13:52
 */

namespace App\Service;


use App\Repository\BaseRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class RepositoryRegistry
{
    private $registry;

    public function __construct(RegistryInterface $registry)
    {
        $this->registry = $registry;
    }

    /**
     * @param $object
     * @return BaseRepository
     */
    public function get($object): BaseRepository
    {
        if (is_object($object)) {
            $object = get_class($object);
        }

        /** @var BaseRepository $repo */
        $repo = $this->registry->getRepository($object);

        return $repo;
    }
}