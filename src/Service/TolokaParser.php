<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 16.02.19
 * Time: 19:57
 */

namespace App\Service;


use App\Entity\MediaItem;
use App\Utils\ParserHandler\Request\Request;

class TolokaParser
{
    /** @var ParsingHandlersChain */
    private $handlersChain;

    /**
     * TolokaParser constructor.
     * @param ParsingHandlersChain $handlersChain
     */
    public function __construct(
        ParsingHandlersChain $handlersChain
    )
    {
        $this->handlersChain = $handlersChain;
    }

    public function getAllTorrentIds(string $content): array
    {
        $matches = [];
        $pattern = "/<a href=\"(?<id>t\d+)/";

        preg_match_all($pattern, $content,$matches);

        $ids = $matches['id'] ?? [];
        $ids = $ids ?: [];

        return array_unique($ids);
    }

    /**
     * @param string $content
     * @return MediaItem
     */
    public function getMediaItem(string $content): MediaItem
    {
        $request = new Request($content);
        $this->handlersChain->handle($request);

        return $request->getMediaItem();
    }
}