<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 13.09.18
 * Time: 13:55
 */

namespace App\Service;

use App\Entity\AFile;
use ReflectionClass;
use Symfony\Component\Filesystem\Filesystem;

class UploadedFilesManager
{
    protected $env;
    protected $rootDir;

    public function __construct($env, $rootDir)
    {
        $this->env = $env;
        $this->rootDir = $rootDir;
    }

    public function upload(AFile $file)
    {
        // the file property can be empty if the field is not required
        if (null === $file->getFile()) {
            return;
        }

        // move takes the target directory and then the
        // target filename to move to
        $file->getFile()->move(
            $this->getUploadDir() . $this->getFileClass($file) . '/',
            $this->getFileNameInPath($file)
        );
    }

    public function remove(AFile $file)
    {
        $fs = new Filesystem();
        
        if ($fs->exists($file->getFilePath())) {
            $fs->remove($file->getFilePath());
        }
    }

    public function preSave(AFile $file)
    {
        $file->setFilePath($this->getFilePath($file));
        $file->setLink($this->getFileLink($file));
    }

    private function getUploadDir() {
        return "{$this->rootDir}/..{$this->getLinkDir()}";
    }

    private function getLinkDir() {
        return "/uploads/{$this->env}/";
    }

    private function getFilePath(AFile $file)
    {
        return $this->getUploadDir() . $this->getFileClass($file) . '/' . $this->getFileNameInPath($file);
    }

    private function getFileLink(AFile $file)
    {
        return $this->getLinkDir() . $this->getFileClass($file) . '/' . $this->getFileNameInPath($file);
    }

    /**
     * Returns relevant file upload directory.
     * @param AFile $file
     * @return mixed
     */
    private function getFileClass(AFile $file): string
    {
        try {
            $reflect = new ReflectionClass($file);
        } catch (\Exception $e) {
            return '';
        }

        return $reflect->getShortName();
    }

    private function getFileNameInPath(AFile $file): string
    {
        return $file->getFileName();
    }
}