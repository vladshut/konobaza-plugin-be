<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 25.12.18
 * Time: 12:46
 */

namespace App\Service;

use Monolog\Logger as MonologLogger;
use Psr\Log\LoggerInterface;

class LoggerFactory
{
    /**
     * @param MonologLogger $logger
     * @return Logger
     */
    public function create(MonologLogger $logger): LoggerInterface
    {
        return new Logger($logger);
    }
}