<?php

namespace App\Entity;

use App\Entity\Traits\DictionaryTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CountryRepository")
 */
class Country implements \JsonSerializable
{
    use DictionaryTrait;
}
