<?php

namespace App\Entity;

use App\Entity\Traits\DictionaryTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QualityRepository")
 */
class Quality implements \JsonSerializable
{
    use DictionaryTrait;
}
