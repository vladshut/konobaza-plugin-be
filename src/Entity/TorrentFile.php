<?php

namespace App\Entity;

use App\Service\Lightbenc;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TorrentFileRepository")
 */
class TorrentFile extends AFile implements JsonSerializable
{
    protected $serializableFields = [
        'magnetLink',
        'infoHash',
        'externalLink',
        'link',
    ];

    public function getMagnetLink()
    {
        return 'magnet:?xt=urn:btih:'. $this->getInfoHash();
    }

    public function getInfoHash()
    {
        $info = $this->filePath ? Lightbenc::bdecodeGetinfo($this->filePath) : null;

        return $info['info_hash'] ?? null;
    }
}
