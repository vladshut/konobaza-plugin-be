<?php

namespace App\Entity;

use App\Entity\Traits\DictionaryTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ActorRepository")
 */
class Actor implements \JsonSerializable
{
    use DictionaryTrait;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nameEn;

    public function getNameEn(): ?string
    {
        return $this->nameEn;
    }

    public function setNameEn(?string $nameEn): self
    {
        $this->nameEn = $nameEn;

        return $this;
    }

    public function getSerializableFields()
    {
        return ['name', 'nameEn'];
    }
}
