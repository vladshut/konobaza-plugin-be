<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 15.08.18
 * Time: 8:20
 */

namespace App\Entity\Traits;

/**
 * Trait DictionaryTrait
 * @package App\Common\Entity
 * @method string|null getName()
 * @method self setName(string $name)
 */
trait DictionaryTrait
{
    use
        IdPropertyTrait,
        NamePropertyTrait,
        SerializableTrait
    ;

    protected $serializableFields = [
        'id',
        'name',
    ];

    public function __toString()
    {
        return $this->alias ?? '';
    }
}
