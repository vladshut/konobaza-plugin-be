<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 21.02.19
 * Time: 16:57
 */

namespace App\Entity\Traits;


trait NamePropertyTrait
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $name;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $name = (strlen($name) > 255) ? substr($name,0,250) : $name;

        $this->name = $name;

        return $this;
    }
}