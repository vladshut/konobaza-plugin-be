<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 21.02.19
 * Time: 16:57
 */

namespace App\Entity\Traits;


trait IdPropertyTrait
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    public function getId(): ?int
    {
        return $this->id;
    }
}