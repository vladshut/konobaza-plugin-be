<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 15.08.18
 * Time: 8:20
 */

namespace App\Entity\Traits;


use DateTime;
use Doctrine\Common\Collections\Collection;
use JsonSerializable;
use PHPUnit\Framework\MockObject\BadMethodCallException;

trait SerializableTrait
{
    public function jsonSerialize()
    {
        $serializableFields = ['id'];

        if (property_exists($this, 'serializableFields')) {
            $serializableFields = array_merge($serializableFields, $this->serializableFields);
            $serializableFields = array_unique($serializableFields);
        }

        if (method_exists($this, 'getSerializableFields')) {
            $serializableFields = array_merge($serializableFields, $this->getSerializableFields());
            $serializableFields = array_unique($serializableFields);
        }

        $json = [];

        foreach ($serializableFields as $key => $serializableField) {
            if (is_numeric($key)) {
                $key = $serializableField;
            }

            $value = $this->getValueByDotNotation($serializableField);

            if ($value instanceof Collection) {
                $array = $value->toArray();

                foreach ($array as &$item) {
                    if ($item instanceof JsonSerializable) {
                        $item = $item->jsonSerialize();
                    }
                }
                unset($item);
                $value = $array;
            } else if ($value instanceof DateTime) {
                $value = $value->format('Y-m-d H:i:s');
            } else if ($value instanceof JsonSerializable) {
                $value = $value->jsonSerialize();
            }

            $json[$key] = $value;
        }

        return $json;
    }

    /**
     * @param string $fieldPath
     * @param null $default
     * @return mixed|null
     */
    public function getValueByDotNotation(string $fieldPath, $default = null)
    {
        if (property_exists($this, $fieldPath)) {
            return $this->$fieldPath;
        }

        $methodName = 'get' . ucfirst($fieldPath);

        if (method_exists($this, $methodName) || method_exists($this, '__call')) {
            try {
                return $this->$methodName();
            } catch (BadMethodCallException $e) {}
        }

        if (strpos($fieldPath, '.') === false) {
            return $default;
        }

        $pathParts = explode('.', $fieldPath);
        $firstPart = array_shift($pathParts);

        if (property_exists($this, $firstPart) && method_exists($this->$firstPart, 'getValueByDotNotation')) {
            $fieldPath = implode(',', $pathParts);

            return $this->$firstPart->getValueByDotNotation($fieldPath, $default);
        }

        return $default;
    }
}