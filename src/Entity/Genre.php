<?php

namespace App\Entity;

use App\Entity\Traits\DictionaryTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GenreRepository")
 */
class Genre implements \JsonSerializable
{
    use DictionaryTrait;
}
