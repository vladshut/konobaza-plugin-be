<?php

namespace App\Entity;

use App\Entity\Traits\IdPropertyTrait;
use App\Entity\Traits\SerializableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MediaItemRepository")
 * @ORM\Table(name="media_item")
 */
class MediaItem implements \JsonSerializable
{
    use IdPropertyTrait, SerializableTrait;

    protected $serializableFields = [
        'imdbId',
        'tolokaTorrentId',
        'torrentFileExternalUrl',
        'titleUa',
        'titleOrig',
        'description',
        'year',
        'duration',
        'size',
        'actors',
        'genres',
        'countries',
        'quality',
        'poster',
        'torrentFile',
    ];

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $tolokaTorrentId;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imdbId;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $imdbRating;

    /**
     * @var TorrentFile
     * @ORM\OneToOne(targetEntity="App\Entity\TorrentFile", cascade={"persist", "remove"})
     */
    private $torrentFile;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titleOrig;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titleUa;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $year;

    /**
     * @var PosterFile
     * @ORM\OneToOne(targetEntity="App\Entity\PosterFile", cascade={"persist", "remove"})
     */
    private $poster;

    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="App\Entity\Genre", cascade={"persist"})
     */
    private $genres;

    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="App\Entity\Country", cascade={"persist"})
     */
    private $countries;

    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="App\Entity\Actor", cascade={"persist"})
     */
    private $actors;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=false, nullable=true)
     */
    private $description;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $duration;

    /**
     * @var Quality
     * @ORM\ManyToOne(targetEntity="App\Entity\Quality", cascade={"persist"})
     */
    private $quality;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $size;

    public function __construct()
    {
        $this->genres = new ArrayCollection();
        $this->actors = new ArrayCollection();
        $this->countries = new ArrayCollection();
    }

    public function getTolokaTorrentId(): ?string
    {
        return $this->tolokaTorrentId;
    }

    public function setTolokaTorrentId(string $tolokaTorrentId): self
    {
        $this->tolokaTorrentId = $tolokaTorrentId;

        return $this;
    }

    public function getImdbId(): ?string
    {
        return $this->imdbId;
    }

    public function setImdbId(?string $imdbId): self
    {
        $this->imdbId = $imdbId;

        return $this;
    }

    public function getTorrentFile(): ?TorrentFile
    {
        return $this->torrentFile;
    }

    public function setTorrentFile(?TorrentFile $torrentFile): self
    {
        $this->torrentFile = $torrentFile;

        return $this;
    }

    /**
     * @return float
     */
    public function getImdbRating(): ?float
    {
        return $this->imdbRating;
    }

    /**
     * @param float $imdbRating
     * @return self
     */
    public function setImdbRating(float $imdbRating): self
    {
        $this->imdbRating = $imdbRating;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitleOrig(): ?string
    {
        return $this->titleOrig;
    }

    /**
     * @param string $titleOrig
     * @return self
     */
    public function setTitleOrig(string $titleOrig): self
    {
        $this->titleOrig = $titleOrig;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitleUa(): ?string
    {
        return $this->titleUa;
    }

    /**
     * @param string $titleUa
     * @return self
     */
    public function setTitleUa(string $titleUa): self
    {
        $this->titleUa = $titleUa;

        return $this;
    }

    /**
     * @return int
     */
    public function getYear(): ?int
    {
        return $this->year;
    }

    /**
     * @param int $year
     * @return self
     */
    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    /**
     * @return PosterFile
     */
    public function getPoster(): ?PosterFile
    {
        return $this->poster;
    }

    /**
     * @param PosterFile $poster
     * @return self
     */
    public function setPoster(?PosterFile $poster): self
    {
        $this->poster = $poster;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getGenres(): ?Collection
    {
        return $this->genres;
    }

    /**
     * @param Collection $genres
     * @return self
     */
    public function setGenres(Collection $genres): self
    {
        $this->genres = $genres;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getCountries(): ?Collection
    {
        return $this->countries;
    }

    /**
     * @param Collection $countries
     * @return self
     */
    public function setCountries(Collection $countries): self
    {
        $this->countries = $countries;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getActors(): ?Collection
    {
        return $this->actors;
    }

    /**
     * @param Collection $actors
     * @return self
     */
    public function setActors(Collection $actors): self
    {
        $this->actors = $actors;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return self
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return int
     */
    public function getDuration(): ?int
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     * @return self
     */
    public function setDuration(int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * @return Quality
     */
    public function getQuality(): ?Quality
    {
        return $this->quality;
    }

    /**
     * @param Quality $quality
     * @return self
     */
    public function setQuality(Quality $quality): self
    {
        $this->quality = $quality;

        return $this;
    }

    /**
     * @return int
     */
    public function getSize(): ?int
    {
        return $this->size;
    }

    /**
     * @param float $size
     * @return self
     */
    public function setSize(float $size): self
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @param Country $country
     * @return MediaItem
     */
    public function addCountry(Country $country): self
    {
        if (!$this->countries->contains($country)) {
            $this->countries->add($country);
        }
        
        return $this;
    }
    
    /**
     * @param Genre $genre
     * @return MediaItem
     */
    public function addGenre(Genre $genre): self
    {
        if (!$this->genres->contains($genre)) {
            $this->genres->add($genre);
        }

        return $this;
    }

    /**
     * @param Actor $actor
     * @return MediaItem
     */
    public function addActor(Actor $actor): self
    {
        if (!$this->actors->contains($actor)) {
            $this->actors->add($actor);
        }

        return $this;
    }

    public function getTorrentFileName()
    {
        $pieces = [$this->titleOrig, "({$this->year})", $this->quality->getName()];
        $name = implode(' ', $pieces);
        $name = str_replace(' ', '_', $name);
        $name .= '.torrent';

        return $name;
    }
}
