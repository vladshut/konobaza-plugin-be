<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TorrentRepository")
 */
class Torrent
{
    const STATE_NEW = 'new';
    const STATE_HANDLED_ERROR = 'handled_error';
    const STATE_HANDLED_SUCCESS = 'handled_success';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tolokaTorrentId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $state = self::STATE_NEW;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTolokaTorrentId(): ?string
    {
        return $this->tolokaTorrentId;
    }

    public function setTolokaTorrentId(string $tolokaTorrentId): self
    {
        $this->tolokaTorrentId = $tolokaTorrentId;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }
}
