<?php

namespace App\Entity;

use App\Entity\Traits\IdPropertyTrait;
use App\Entity\Traits\SerializableTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity
 * @ORM\Table(name="file")
 * @ORM\InheritanceType("SINGLE_TABLE")
 */
abstract class AFile implements \JsonSerializable
{
    use IdPropertyTrait, SerializableTrait;

    protected $serializableFields = [
        'externalLink',
        'link',
    ];

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $fileName;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $link;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $extension;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    protected $size;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $dtCreated;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $isTemp;

    /**
     * @var File
     */
    protected $file;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $filePath;

    /**
     * @var string
     */
    protected $externalLink;

    /**
     * Sets file.
     *
     * @param File $file
     * @return AFile Current object.
     * @throws \Exception
     */
    public function setFile(File $file): self
    {
        $this->file = $file;
        $this->extension = $file->guessExtension() ?: $file->getExtension();
        $this->size = $file->getSize();
        $this->fileName = $this->file->getFilename();
        $this->dtCreated = new \DateTime();
        $this->isTemp = false;

        return $this;
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     * @return AFile
     */
    public function setFileName(string $fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string
     */
    public function getFileName(): string
    {
        return (string)$this->fileName;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return AFile
     */
    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * Set extension
     *
     * @param string $extension
     * @return AFile
     */
    public function setExtension(string $extension): self
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get extension
     *
     * @return string
     */
    public function getExtension(): ?string
    {
        return $this->extension;
    }

    /**
     * Set size
     *
     * @param float $size
     * @return AFile
     */
    public function setSize(float $size): self
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return string
     */
    public function getSize(): ?string
    {
        return $this->size;
    }

    /**
     * Set dtCreated
     *
     * @param \DateTime $dtCreated
     * @return AFile
     */
    public function setDtCreated(\DateTime $dtCreated): self
    {
        $this->dtCreated = $dtCreated;

        return $this;
    }

    /**
     * Get dtCreated
     *
     * @return \DateTime
     */
    public function getDtCreated(): ?\DateTime
    {
        return $this->dtCreated;
    }

    /**
     * Get file.
     *
     * @return File
     */
    public function getFile(): ?File
    {
        return $this->file;
    }

    /**
     * Set isTemp
     *
     * @param boolean $isTemp
     * @return AFile
     */
    public function setIsTemp(bool $isTemp): self
    {
        $this->isTemp = $isTemp;

        return $this;
    }

    /**
     * Get isTemp
     *
     * @return boolean
     */
    public function getIsTemp(): ?bool
    {
        return $this->isTemp;
    }

    /**
     * Generates file name.
     * @return string
     */
    protected function generateName(): string
    {
        return md5(time() . $this->file->getFilename()) . '.' . $this->getExtension();
    }

    /**
     * Set filePath
     *
     * @param string $filePath
     * @return AFile
     */
    public function setFilePath(string $filePath): self
    {
        $this->filePath = $filePath;

        return $this;
    }

    /**
     * Get filePath
     *
     * @return string
     */
    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    /**
     * @return string
     */
    public function getExternalLink(): string
    {
        return $this->externalLink;
    }

    /**
     * @param string $externalLink
     * @return $this
     */
    public function setExternalLink(string $externalLink): self
    {
        $this->externalLink = $externalLink;

        return $this;
    }

    public function getContent()
    {
        return mb_convert_encoding(file_get_contents($this->filePath), "UTF-8", "UTF-8");
    }
}
