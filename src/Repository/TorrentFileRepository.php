<?php

namespace App\Repository;

use App\Entity\TorrentFile;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TorrentFile|null find($id, $lockMode = null, $lockVersion = null)
 * @method TorrentFile|null findOneBy(array $criteria, array $orderBy = null)
 * @method TorrentFile[]    findAll()
 * @method TorrentFile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TorrentFileRepository extends BaseRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TorrentFile::class);
    }

    // /**
    //  * @return TorrentFile[] Returns an array of TorrentFile objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TorrentFile
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
