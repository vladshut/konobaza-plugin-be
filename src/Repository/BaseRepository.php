<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 23.02.19
 * Time: 22:31
 */

namespace App\Repository;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bridge\Doctrine\RegistryInterface;

class BaseRepository extends ServiceEntityRepository
{
    protected $entityClass;
    private $registry;

    public function __construct(RegistryInterface $registry, $entityClass = null)
    {
        $this->registry = $registry;
        $this->entityClass = $entityClass;
        parent::__construct($registry, $this->entityClass);
    }

    public function save($entity)
    {
        if (!is_array($entity)) {
            $entity = [$entity];
        }

        foreach ($entity as $singleEntity) {
            $this->saveSingle($singleEntity);
        }
    }

    public function remove($entity)
    {
        if (!is_array($entity)) {
            $entity = [$entity];
        }

        foreach ($entity as $singleEntity) {
            $this->removeSingle($singleEntity);
        }
    }

    /**
     * @param $entity
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function saveSingle($entity)
    {
        $this->_em->persist($entity);
        $this->_em->flush($entity);
    }

    /**
     * @param $entity
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws EntityNotFoundException
     */
    private function removeSingle($entity)
    {
        if (is_string($entity)) {
            $entity = $this->get($entity);
        }

        $this->_em->remove($entity);
        $this->_em->flush();
    }

    public function getDictionary()
    {
        $dictionary = [];
        $entities = $this->findAll();

        foreach ($entities as $entity) {
            $dictionary[] = ['id' => $entity->getId(), 'name' => $entity->getName() ?? $entity->getAlias()];
        }

        return $dictionary;
    }

    /**
     * @param $object
     * @return BaseRepository
     */
    protected function getRepo($object): BaseRepository
    {
        if (is_object($object)) {
            $object = get_class($object);
        }

        /** @var BaseRepository $repo */
        $repo = $this->registry->getRepository($object);

        return $repo;
    }

    /**
     * @param $id
     * @param null $lockMode
     * @param null $lockVersion
     * @return null|object
     * @throws EntityNotFoundException
     */
    public function get($id, $lockMode = null, $lockVersion = null)
    {
        $entity = $this->find($id, $lockMode, $lockVersion);

        if (is_null($entity)) {
            throw new EntityNotFoundException($this->_entityName, compact('id'));
        }

        return $entity;
    }

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @return null|object
     * @throws EntityNotFoundException
     */
    public function getOneBy(array $criteria, array $orderBy = null)
    {
        $entity = $this->findOneBy($criteria, $orderBy);

        if (is_null($entity)) {
            throw new EntityNotFoundException($this->_entityName, compact('criteria'));
        }

        return $entity;
    }

    public function findByOrCreate($criteria)
    {
        $entity = $this->findOneBy($criteria);

        if (is_null($entity)) {
            $entity = $this->create();

            foreach ($criteria as $key => $value) {
                $this->setValue($entity, $key, $value);
            }
        }

        return $entity;
    }

    public function create() {
        return (new $this->_entityName);
    }

    /**
     * @param $entity
     * @throws \Doctrine\ORM\ORMException
     */
    public function refresh($entity)
    {
        $this->getEntityManager()->refresh($entity);
    }

    protected function setValue($entity, $key, $value)
    {
        $methodName = 'set' . ucfirst($key);
        if (method_exists($entity, $methodName)) {
            $entity->$methodName($value);
        }

        return $entity;
    }
}