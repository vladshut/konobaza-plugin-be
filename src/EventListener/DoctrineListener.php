<?php

namespace App\EventListener;

use App\Service\UploadedFilesManager;
use App\Entity\AFile;
use Doctrine\ORM\Event\LifecycleEventArgs;

class DoctrineListener
{
    /** @var UploadedFilesManager */
    protected $uploadedFilesManager;

    /**
     * DoctrineListener constructor.
     * @param $uploadedFilesManager
     */
    public function __construct(UploadedFilesManager $uploadedFilesManager)
    {
        $this->uploadedFilesManager = $uploadedFilesManager;
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof AFile) {
            $this->uploadedFilesManager->preSave($entity);
        }
    }


    public function postPersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof AFile) {
            $this->uploadedFilesManager->upload($entity);
        }
    }

    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof AFile) {
            $this->uploadedFilesManager->remove($entity);
        }
    }
}