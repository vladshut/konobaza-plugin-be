<?php
/**
 * Created by PhpStorm.
 * User: vs
 * Date: 03.03.19
 * Time: 13:19
 */

namespace App\Controller;


use App\Entity\MediaItem;
use App\Repository\MediaItemRepository;
use App\Service\TorrentInfoScraper;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityNotFoundException;
use Exception;
use Psr\Cache\CacheItemInterface;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MediaController extends AbstractController
{
    /**
     * @Route("/api/media", methods={"GET", "POST"})
     * @param Request $request
     * @param MediaItemRepository $mediaItemRepository
     * @param TorrentInfoScraper $scraper
     * @param CacheItemPoolInterface $cache
     * @return JsonResponse
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function find(Request $request, MediaItemRepository $mediaItemRepository, TorrentInfoScraper $scraper, CacheItemPoolInterface $cache)
    {
        $imdbId = $request->get('imdbId', null);
        $title = $request->get('title', null);

        if (!$imdbId && !$title) {
            return new JsonResponse([], Response::HTTP_NO_CONTENT);
        }

        $expr = Criteria::expr();
        $criteria = Criteria::create();

        if ($imdbId && $title) {
            $criteria->where(
                $expr->orX(
                    $expr->eq('imdbId', $imdbId),
                    $expr->andX(
                        $expr->eq('titleOrig', $title),
                        $expr->isNull('imdbId')
                    )
                )
            );
        } else if ($imdbId && !$title) {
            $criteria->where($expr->eq('imdbId', $imdbId));
        } else if (!$imdbId && $title) {
            $criteria->where($expr->eq('titleOrig', $title));
        }

        $itemsObj = $mediaItemRepository->matching($criteria)->toArray();
        $items = [];

        $fields = ['titleOrig', 'titleUa', 'imdbId', 'torrentFile', 'size', 'quality', 'year', 'poster', 'id', 'tolokaTorrentId'];
        /** @var MediaItem $itemObj */
        foreach ($itemsObj as $itemObj) {
            $item = array_only($itemObj->jsonSerialize(), $fields);

            if (isset($item['torrentFile']['infoHash'])) {
                $hash = $item['torrentFile']['infoHash'];

                /** @var CacheItemInterface $item */
                $cachedData = $cache->getItem($hash);

                if (!$cachedData->isHit()) {
                    $hashes = [$hash];
                    $trackers = ['http://bt.toloka.to/announce'];

                    $info = $scraper->scrape($hashes, $trackers);

                    /** @var CacheItemInterface $item */
                    $cachedData = $cache->getItem($hash);

                    if (empty($scraper->get_errors())) {
                        $cachedData->set($info[$hash]);
                        $item['torrentFile'] = $item['torrentFile'] + $info[$hash];
                    } else {
                        $cachedData->set([]);
                    }

                    $cachedData->expiresAfter(3600);
                    $cache->save($cachedData);
                } else {
                    $item['torrentFile'] = $item['torrentFile'] + $cachedData->get();
                }
            }

            $items[] = $item;
        }

        usort($items, function($a, $b) {
            $a = $a['torrentFile']['seeders'] ?? 0;
            $b = $b['torrentFile']['seeders'] ?? 0;

            return $a == $b ? 0 : (($a > $b) ? -1 : 1);
        });

        return $this->json($items);
    }

    /**
     * @Route("/uploads/{env}/{type}/{name}", methods={"GET"})
     * @param Request $request
     * @param string $env
     * @param string $type
     * @param string $name
     * @return BinaryFileResponse
     */
    public function downloadFile(Request $request, string $env, string $type, string $name)
    {
        $fileName = $request->get('fileName', null);
        $path = __DIR__ . "/../../uploads/{$env}/{$type}/{$name}";

        return $this->file($path, $fileName);
    }

    /**
     * @Route("/api/media/{id}/download", methods={"GET"})
     * @param string $id
     * @param MediaItemRepository $mediaItemRepository
     * @return BinaryFileResponse
     * @throws EntityNotFoundException
     */
    public function download(string $id, MediaItemRepository $mediaItemRepository)
    {
        /** @var MediaItem $media */
        $media = $mediaItemRepository->get($id);

        $path = $media->getTorrentFile()->getLink();
        $fileName = $media->getTorrentFileName();

        $path = __DIR__ . "/../..{$path}";

        return $this->file($path, $fileName);
    }
}